### 下载

`Broker`的下载和`NameServer`是在一起的

### 启动`Broker`

```
> cd rocketmq-all-4.9.0-bin-release/
> nohup sh bin/mqbroker -n localhost:9876 &
> tail -f ~/logs/rocketmqlogs/broker.log 
  The broker[%s, 172.30.30.233:10911] boot success...
```

#### `Broker`启动脚本分析

通`NameServer`启动一样的，最终是执行 bin/ 目录下的`runbroker.sh`，传入了最终要启动的`org.apache.rocketmq.broker.BrokerStartup`启动类

```
# mqbroker.sh

...
sh ${ROCKETMQ_HOME}/bin/runbroker.sh org.apache.rocketmq.broker.BrokerStartup $@
```

`runbroker.sh`主要作用：

- 指定了`${BASE_DIR}`，然后覆盖指定了`-Djava.ext.dirs`启动参数，使得`ExtClassLoader类加载器`会加载`${ROCKETMQ_HOME}/lib`目录下的的jar，即 rocketmq 启动时的相关依赖

- 设置`GC`的相关配置参数
- 指定`JVM`的启动参数

### 停止`Broker`

```
> sh bin/mqshutdown broker
The mqbroker(36695) is running...
Send shutdown request to mqbroker(36695) OK
```

`shutdown`操作就是找到进程pid，并执行`kill`操作

`Broker`启动的时候注册了JVM的ShutdownHook，执行`BrokerController`的关闭操作

### `Broker`启动配置

`Broker`的启动时，支持的命令行参数有`-h`、`-n`、`-p`、`-c`、`-m`（相关逻辑在`org.apache.rocketmq.broker.BrokerStartup`类中）:
- `-h`参数（'Print help'）
- `-n`参数（'namesrvAddr'）

    指定`brokerServer`的地址
- `-p`参数（'Print all config item'）
    打印所有的配置参数  
- `-m`参数（'Print important config item'）
    
    打印重要的配置参数，具体参数截取如下：
    ```
    2021-08-07 22\:32\:16 INFO main - namesrvAddr=localhost:9876
    2021-08-07 22\:32\:16 INFO main - brokerIP1=10.0.8.3
    2021-08-07 22\:32\:16 INFO main - brokerName=VM-8-3-centos
    2021-08-07 22\:32\:16 INFO main - brokerClusterName=DefaultCluster
    2021-08-07 22\:32\:16 INFO main - brokerId=0
    2021-08-07 22\:32\:16 INFO main - autoCreateTopicEnable=true
    2021-08-07 22\:32\:16 INFO main - autoCreateSubscriptionGroup=true
    2021-08-07 22\:32\:16 INFO main - msgTraceTopicName=RMQ_SYS_TRACE_TOPIC
    2021-08-07 22\:32\:16 INFO main - traceTopicEnable=false
    2021-08-07 22\:32\:16 INFO main - rejectTransactionMessage=false
    2021-08-07 22\:32\:16 INFO main - fetchNamesrvAddrByAddressServer=false
    2021-08-07 22\:32\:16 INFO main - transactionTimeOut=6000
    2021-08-07 22\:32\:16 INFO main - transactionCheckMax=15
    2021-08-07 22\:32\:16 INFO main - transactionCheckInterval=60000
    2021-08-07 22\:32\:16 INFO main - aclEnable=false
    2021-08-07 22\:32\:16 INFO main - storePathRootDir=/root/store
    2021-08-07 22\:32\:16 INFO main - storePathCommitLog=/root/store/commitlog
    2021-08-07 22\:32\:16 INFO main - flushIntervalCommitLog=500
    2021-08-07 22\:32\:16 INFO main - commitIntervalCommitLog=200
    2021-08-07 22\:32\:16 INFO main - flushCommitLogTimed=false
    2021-08-07 22\:32\:16 INFO main - deleteWhen=04
    2021-08-07 22\:32\:16 INFO main - fileReservedTime=72
    2021-08-07 22\:32\:16 INFO main - maxTransferBytesOnMessageInMemory=262144
    2021-08-07 22\:32\:16 INFO main - maxTransferCountOnMessageInMemory=32
    2021-08-07 22\:32\:16 INFO main - maxTransferBytesOnMessageInDisk=65536
    2021-08-07 22\:32\:16 INFO main - maxTransferCountOnMessageInDisk=8
    2021-08-07 22\:32\:16 INFO main - accessMessageInMemoryMaxRatio=40
    2021-08-07 22\:32\:16 INFO main - messageIndexEnable=true
    2021-08-07 22\:32\:16 INFO main - messageIndexSafe=false
    2021-08-07 22\:32\:16 INFO main - haMasterAddress=
    2021-08-07 22\:32\:16 INFO main - brokerRole=ASYNC_MASTER
    2021-08-07 22\:32\:16 INFO main - flushDiskType=ASYNC_FLUSH
    2021-08-07 22\:32\:16 INFO main - cleanFileForciblyEnable=true
    2021-08-07 22\:32\:16 INFO main - transientStorePoolEnable=false
    ```

- `-c`参数（'Name server config properties file'）

    执行服务启动的配置文件，文件的配置会对应到`org.apache.rocketmq.common.BrokerConfig`、`org.apache.rocketmq.remoting.netty.NettyServerConfig`、`org.apache.rocketmq.remoting.netty.NettyClientConfig`、`org.apache.rocketmq.store.config.MessageStoreConfig`四个配置，具体参数比较多，在broker源码阅读中具体理解

    简单的例如说需要修改`Broker`的监听端口，也是修改`listenPort`配置项
    ```
    listenPort=8888
    ```
    并在启动中指定配置文件即可