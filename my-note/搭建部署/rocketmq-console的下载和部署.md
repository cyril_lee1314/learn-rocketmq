
- github官方文档： [https://github.com/apache/rocketmq-externals/tree/master/rocketmq-console/doc/1_0_0](https://github.com/apache/rocketmq-externals/tree/master/rocketmq-console/doc/1_0_0)

- `rocketmq-console`在`apache/rocketmq-externals`的github项目中，稳定版本只有`1.0.0`，但是它文档里的示例用的是`2.0.0`版本，不太清楚了，我这里还是使用的最新的`2.0.0`了。

#### 下载源码

    这里使用的是我自己在`gitee`上导入过来的镜像仓库
    ```
    > git clone git@github.com:apache/rocketmq-externals.git
    ```

#### 配置`nameserver address`
    修改`rocketmq-externals/rocketmq-consolesrc/main/resources/application.properties`文件的`rocketmq.config.namesrvAddr=`配置

#### 打包并启动
    ```
    > mvn clean package -Dmaven.test.skip=true
    > java -jar target/rocketmq-console-ng-2.0.0.jar
    ```
#### 设置登陆密码

这里直接引用 [官方仓库的文档](https://github.com/apache/rocketmq-externals/blob/master/rocketmq-console/doc/1_0_0/UserGuide_CN.md)：

- 1.在Spring配置文件resources/application.properties中修改 开启登录功能
    ```
    # 开启登录功能
    rocketmq.config.loginRequired=true

    # Dashboard文件目录，登录用户配置文件所在目录
    rocketmq.config.dataPath=/tmp/rocketmq-console/data
    ```

- 2.确保${rocketmq.config.dataPath}定义的目录存在，并且该目录下创建登录配置文件"users.properties", 如果该目录下不存在此文件，则默认使用resources/users.properties文件。 users.properties文件格式为:

    ```
    # 该文件支持热修改，即添加和修改用户时，不需要重新启动console
    # 格式， 每行定义一个用户， username=password[,N]  #N是可选项，可以为0 (普通用户)； 1 （管理员）  

    #定义管理员 
    admin=admin,1

    #定义普通用户
    user1=user1
    user2=user2
    ```
- 3.启动控制台则开启了登录功能