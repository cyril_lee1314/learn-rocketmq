### 下载
- 官方文档： [https://www.apache.org/dyn/closer.cgi?path=rocketmq/4.9.0/rocketmq-all-4.9.0-source-release.zip](https://www.apache.org/dyn/closer.cgi?path=rocketmq/4.9.0/rocketmq-all-4.9.0-source-release.zip)

`国内可以使用清华镜像下载更快：`
 [https://mirrors.tuna.tsinghua.edu.cn/](https://mirrors.tuna.tsinghua.edu.cn/)

`官网 QuickStart 介绍的是source方式下载，这里是直接采用bin方式下载`

```
> wget https://mirrors.tuna.tsinghua.edu.cn/apache/rocketmq/4.9.0/rocketmq-all-4.9.0-bin-release.zip
> unzip rocketmq-all-4.9.0-bin-release.zip
```

### 启动`NameServer`

```
> cd rocketmq-all-4.9.0-bin-release/
> nohup sh bin/mqnamesrv &
> tail -f ~/logs/rocketmqlogs/namesrv.log
  The Name Server boot success...
```

#### `NameServer`启动脚本分析

由上面启动命令可知，实际执行的是 bin/ 目录下的`mqnamesrv.sh`脚本，`mqnamesrv.sh`脚本实际是确定`${ROCKETMQ_HOME}`变量后启动 bin/ 目录下的`runserver.sh`，传入了`org.apache.rocketmq.namesrv.NamesrvStartup`启动类

```
# mqnamesrv.sh

...
sh ${ROCKETMQ_HOME}/bin/runserver.sh org.apache.rocketmq.namesrv.NamesrvStartup $@
```

`runserver.sh`主要作用：

- 指定了`${BASE_DIR}`，然后覆盖指定了`-Djava.ext.dirs`启动参数，使得`ExtClassLoader类加载器`会加载`${ROCKETMQ_HOME}/lib`目录下的的jar，即 rocketmq 启动时的相关依赖
    ```
    # runserver.sh

    ...
    JAVA_OPT="${JAVA_OPT} -Djava.ext.dirs=${JAVA_HOME}/jre/lib/ext:${BASE_DIR}/lib:${JAVA_HOME}/lib/ext"
    ```

- 设置`GC`的相关配置参数
    ```
    # runserver.sh

    ...
    choose_gc_options()
    {   
        # Example of JAVA_MAJOR_VERSION value : '1', '9', '10', '11', ...
        # '1' means releases befor Java 9
        JAVA_MAJOR_VERSION=$("$JAVA" -version 2>&1 | sed -r -n 's/.* version "([0-9]*).*$/\1/p')
        if [ -z "$JAVA_MAJOR_VERSION" ] || [ "$JAVA_MAJOR_VERSION" -lt "9" ] ; then
        JAVA_OPT="${JAVA_OPT} -XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:CMSInitiatingOccupancyFraction=70 -XX:+CMSParallelRemarkEnabled -XX:SoftRefLRUPolicyMSPerMB=0 -XX:+CMSClassUnloadingEnabled -XX:SurvivorRatio=8 -XX:-UseParNewGC"
        JAVA_OPT="${JAVA_OPT} -verbose:gc -Xloggc:${GC_LOG_DIR}/rmq_srv_gc_%p_%t.log -XX:+PrintGCDetails"   
        JAVA_OPT="${JAVA_OPT} -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=5 -XX:GCLogFileSize=30m"   
        else
        JAVA_OPT="${JAVA_OPT} -XX:+UseG1GC -XX:G1HeapRegionSize=16m -XX:G1ReservePercent=25 -XX:InitiatingHeapOccupancyPercent=30 -XX:SoftRefLRUPolicyMSPerMB=0"
        JAVA_OPT="${JAVA_OPT} -Xlog:gc*:file=${GC_LOG_DIR}/rmq_srv_gc_%p_%t.log:time,tags:filecount=5,filesize=30M"
        fi
    }
    ```

- 指定`JVM`的启动参数

```
JAVA_OPT="${JAVA_OPT} -server -Xms4g -Xmx4g -Xmn2g -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
choose_gc_options
JAVA_OPT="${JAVA_OPT} -XX:-OmitStackTraceInFastThrow"
JAVA_OPT="${JAVA_OPT} -XX:-UseLargePages"
```

### 停止`NameServer`

```
> sh bin/mqshutdown broker
The mqbroker(36695) is running...
<!-- Send shutdown request to mqbroker(36695) OK -->
```

`shutdown`操作就是找到进程pid，并执行`kill`操作

`nameServer`启动的时候注册了JVM的ShutdownHook，执行`NamesrvController`的关闭操作

```
    public static NamesrvController start(final NamesrvController controller) throws Exception {
        ...
        Runtime.getRuntime().addShutdownHook(new ShutdownHookThread(log, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                controller.shutdown();
                return null;
            }
        }));

        controller.start();

        return controller;
    }
```

### `NameServer`启动配置

`NameServer`的启动时，支持的命令行参数有`-h`、`-n`、`-p`、`-c`（相关逻辑在`org.apache.rocketmq.namesrv.NamesrvStartup`类中）:
- `-h`参数（'Print help'）
- `-n`参数（'namesrvAddr'）

    指定`nameserver`的地址
- `-p`参数（'Print all config item'）
    
    打印所有的配置参数，具体参数截取如下：
    ```
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - kvConfigPath=/root/namesrv/kvConfig.json
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - configStorePath=/root/namesrv/namesrv.properties
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - productEnvName=center
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - clusterTest=false
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - orderMessageEnable=false
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - listenPort=9876
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - serverWorkerThreads=8
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - serverCallbackExecutorThreads=0
    14:37:02.300 [main] INFO  RocketmqNamesrvConsole - serverSelectorThreads=3
    14:37:02.301 [main] INFO  RocketmqNamesrvConsole - serverOnewaySemaphoreValue=256
    14:37:02.301 [main] INFO  RocketmqNamesrvConsole - serverAsyncSemaphoreValue=64
    14:37:02.301 [main] INFO  RocketmqNamesrvConsole - serverChannelMaxIdleTimeSeconds=120
    14:37:02.301 [main] INFO  RocketmqNamesrvConsole - serverSocketSndBufSize=65535
    14:37:02.301 [main] INFO  RocketmqNamesrvConsole - serverSocketRcvBufSize=65535
    14:37:02.301 [main] INFO  RocketmqNamesrvConsole - serverPooledByteBufAllocatorEnable=true
    14:37:02.301 [main] INFO  RocketmqNamesrvConsole - useEpollNativeSelector=false
    ```
- `-c`参数（'Name server config properties file'）

    执行服务启动的配置文件，文件的配置会对应到`org.apache.rocketmq.common.namesrv.NamesrvConfig`和`org.apache.rocketmq.remoting.netty.NettyServerConfig`两个配置。

    例如说需要修改`NameServer`的监听端口，则只需要在配置文件中设置：
    ```
    listenPort=8888
    ```
    并在启动中指定配置文件即可