
`NameServer`的作用是比较简单的，在[官方的架构介绍](../../rocketmq-4.9.0-source/docs/cn/architecture.md)里有介绍到，摘取如下：

    NameServer：NameServer是一个非常简单的Topic路由注册中心，其角色类似Dubbo中的zookeeper，支持Broker的动态注册与发现。主要包括两个功能：Broker管理，NameServer接受Broker集群的注册信息并且保存下来作为路由信息的基本数据。然后提供心跳检测机制，检查Broker是否还存活；路由信息管理，每个NameServer将保存关于Broker集群的整个路由信息和用于客户端查询的队列信息。然后Producer和Conumser通过NameServer就可以知道整个Broker集群的路由信息，从而进行消息的投递和消费。NameServer通常也是集群的方式部署，各实例间相互不进行信息通讯。Broker是向每一台NameServer注册自己的路由信息，所以每一个NameServer实例上面都保存一份完整的路由信息。当某个NameServer因某种原因下线了，Broker仍然可以向其它NameServer同步其路由信息，Producer,Consumer仍然可以动态感知Broker的路由的信息。

通过阅读代码，个人理解`NameServer`其实就是管理了`cluster`、`broker`、`topic`的信息，`BrokerServer`启动后注册到`NameServer`中，然后`Producer`和`Consumer`都可以调用`NameServer`获取到所有到`Broker`信息，进行消息的生产和消费。

下面是几个主要类的个人解析：

- [NamesrvStartup](./NamesrvStartup.md)
- [NamesrvController](./NamesrvController.md)
- [DefaultRequestProcessor](./DefaultRequestProcessor.md)
- [RouteInfoManager](./RouteInfoManager.md)
