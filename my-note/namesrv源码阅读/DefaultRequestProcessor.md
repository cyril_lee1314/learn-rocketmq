[org.apache.rocketmq.namesrv.processor.DefaultRequestProcessor](../../rocketmq-4.9.0-source/namesrv/src/main/java/org/apache/rocketmq/namesrv/processor/DefaultRequestProcessor.java)

`rocketmq`中将每一个`NettyServer`接收到的请求封装成了一个`RemotingCommand`，里面主要包含了`requestCode`、`requestHeaders`、`requestBody`等信息

`DefaultRequestProcess`就是用来处理这些请求的，根据不同的`requestCode`指定对应的操作

核心方法截取如下：
```
    @Override
    public RemotingCommand processRequest(ChannelHandlerContext ctx,
        RemotingCommand request) throws RemotingCommandException {

        if (ctx != null) {
            log.debug("receive request, {} {} {}",
                request.getCode(),
                RemotingHelper.parseChannelRemoteAddr(ctx.channel()),
                request);
        }


        switch (request.getCode()) {
            /**
             * CYRIL-NOTE: kvConfig的改、查、删操作
             * kvConfig实际是交由{@link org.apache.rocketmq.namesrv.kvconfig.KVConfigManager}管理的
             */
            case RequestCode.PUT_KV_CONFIG:
                return this.putKVConfig(ctx, request);
            case RequestCode.GET_KV_CONFIG:
                return this.getKVConfig(ctx, request);
            case RequestCode.DELETE_KV_CONFIG:
                return this.deleteKVConfig(ctx, request);

            /**
             * CYRIL-NOTE: todo 这一块未能很理解，做的事情是查询某一个broker的配置是否变更，还未清楚哪里会用到
             * 会传一个brokerAddr， 比对一个brokerAddr对应配置的DataVersion，查看配置是否变更
             */
            case RequestCode.QUERY_DATA_VERSION:
                return queryBrokerTopicConfig(ctx, request);

            /**
             * CYRIL-NOTE: broker的注册和下线
             * broker的注册和下线需要分别添加/删除broker相关的信息，
             * 包括brokerLiveTable、filterServerTable、brokerAddrTable、clusterAddrTable、topicQueueTable
             */
            case RequestCode.REGISTER_BROKER:
                Version brokerVersion = MQVersion.value2Version(request.getVersion());
                if (brokerVersion.ordinal() >= MQVersion.Version.V3_0_11.ordinal()) {
                    return this.registerBrokerWithFilterServer(ctx, request);
                } else {
                    return this.registerBroker(ctx, request);
                }
            case RequestCode.UNREGISTER_BROKER:
                return this.unregisterBroker(ctx, request);

            /**
             * CYRIL-NOTE: 获取一个topic的路由信息
             * 主要包括了：
             * - List<QueueData> queueDataList 该topic的所有MessageQueue的信息
             * - List<BrokerData> brokerDataList topic所有MessageQueue所在broker的信息
             * - HashMap<String, List<String>> filterServerMap todo 暂未理解是干吗用的
             */
            case RequestCode.GET_ROUTEINFO_BY_TOPIC:
                return this.getRouteInfoByTopic(ctx, request);

            /**
             * CYRIL-NOTE: 获取所有的broker配置信息和cluster配置信息
             * 分别是存放在 RouteInfoManager.brokerAddrTable 和 RouteInfoManager.clusterAddrTable
             * clusterAddrTable的数据是会在broker注册的时候也添加对应的cluster信息
             */
            case RequestCode.GET_BROKER_CLUSTER_INFO:
                return this.getBrokerClusterInfo(ctx, request);

            /**
             * CYRIL-NOTE: 去除某个broker的写权限
             * 队列定义有读和写两个权限，2=2的1次方=读权限，4=2的2次方=写权限，6=2+4=读写
             * 见{@link QueueData.perm}
             * 这里其实是找到在这个broker中的所有队列，修改其写权限
             */
            case RequestCode.WIPE_WRITE_PERM_OF_BROKER:
                return this.wipeWritePermOfBroker(ctx, request);

            /**
             * CYRIL-NOTE: 获取所有的topic的信息（这里就返回所有的 topic name）
             * 见{@link org.apache.rocketmq.namesrv.routeinfo.RouteInfoManager.topicQueueTable}
             */
            case RequestCode.GET_ALL_TOPIC_LIST_FROM_NAMESERVER:
                return getAllTopicListFromNameserver(ctx, request);

            /**
             * CYRIL-NOTE: 移除topic
             * 见{@link org.apache.rocketmq.namesrv.routeinfo.RouteInfoManager.topicQueueTable}
             */
            case RequestCode.DELETE_TOPIC_IN_NAMESRV:
                return deleteTopicInNamesrv(ctx, request);

            /**
             * CYRIL-NOTE: 获取某个namespace的kvConfig配置信息
             */
            case RequestCode.GET_KVLIST_BY_NAMESPACE:
                return this.getKVListByNamespace(ctx, request);

            /**
             * CYRIL-NOTE: 获取某个cluster的所有topic信息
             * 先根据cluster找到它所有的broker，在找到broker的所有topic
             */
            case RequestCode.GET_TOPICS_BY_CLUSTER:
                return this.getTopicsByCluster(ctx, request);

            /**
             * CYRIL-NOTE: 获取所有系统topic的信息
             * 查看代码，其实返回的是所有的 clusterName、所有的 brokerName 和 brokerAddr
             */
            case RequestCode.GET_SYSTEM_TOPIC_LIST_FROM_NS:
                return this.getSystemTopicListFromNs(ctx, request);

            /**
             * CYRIL-NOTE: 获取UnitFlag的所有topic
             * todo 还未能理解 unit_topic 这个概念
             * 实际是每个{@link QueueData}有一个'topicSysFlag'字段，
             * 这里是找到所有 存在队列的topicSysFlag是UnitFlag 的所有topic
             */
            case RequestCode.GET_UNIT_TOPIC_LIST:
                return this.getUnitTopicList(ctx, request);

            /**
             * CYRIL-NOTE: 获取UnitSubFlag的所有topic
             */
            case RequestCode.GET_HAS_UNIT_SUB_TOPIC_LIST:
                return this.getHasUnitSubTopicList(ctx, request);

            /**
             * CYRIL-NOTE: 同上 !hasUnitFlag && hasUnitSubFlag
             */
            case RequestCode.GET_HAS_UNIT_SUB_UNUNIT_TOPIC_LIST:
                return this.getHasUnitSubUnUnitTopicList(ctx, request);

            /**
             * CYRIL-NOTE: 更新 nameserver 的配置
             * 见{@link org.apache.rocketmq.common.Configuration.configObjectList}
             */
            case RequestCode.UPDATE_NAMESRV_CONFIG:
                return this.updateConfig(ctx, request);

            /**
             * CYRIL-NOTE: 获取 nameserver 的配置
             * 见{@link org.apache.rocketmq.common.Configuration.configObjectList}
             */
            case RequestCode.GET_NAMESRV_CONFIG:
                return this.getConfig(ctx, request);
            default:
                break;
        }
        return null;
    }

```
