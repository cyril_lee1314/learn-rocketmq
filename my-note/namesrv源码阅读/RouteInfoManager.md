[org.apache.rocketmq.namesrv.routeinfo.RouteInfoManager](../../rocketmq-4.9
.0-source/namesrv/src/main/java/org/apache/rocketmq/namesrv/routeinfo/RouteInfoManager.java)


管理的就是的就是`NameServer`需提供的所有配置信息，核心代码截取如下：

```
    public class RouteInfoManager {

        ...

        /**
         * CYRIL-NOTE: 并发控制的一个读写锁，在查询/修改配置时分别使用读/写锁
         */
        private final ReadWriteLock lock = new ReentrantReadWriteLock();
        /**
         * CYRIL-NOTE: 存放所有 topic 对应的物理队列的信息
         */
        private final HashMap<String/* topic */, List<QueueData>> topicQueueTable;
        /**
         * CYRIL-NOTE: 存放所有 brokerName 对应的 BrokerData
         * 一个 BrokerData 可能包含多个 BrokerAddr， BrokerData指代逻辑上的整体Broker，用BrokerAddr指代具体某个Broker
         * 一个 BrokerAddr 指代一个JVM实例，Broker内部根据BrokerID区分主从，0为主，其他为从
         */
        private final HashMap<String/* brokerName */, BrokerData> brokerAddrTable;
        /**
         * CYRIL-NOTE: 存放 clusterName 对应其下面的 brokerName 信息
         */
        private final HashMap<String/* clusterName */, Set<String/* brokerName */>> clusterAddrTable;
        /**
         * CYRIL-NOTE: 存放各个 brokerAddr 对应的broker信息
         * brokerAddr的上线和下线会更新该配置
         */
        private final HashMap<String/* brokerAddr */, BrokerLiveInfo> brokerLiveTable;
        /**
         * CYRIL-NOTE: FilterServer 暂未理解
         */
        private final HashMap<String/* brokerAddr */, List<String>/* Filter Server */> filterServerTable;

        ...
    }
```
