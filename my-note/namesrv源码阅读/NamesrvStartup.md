[org.apache.rocketmq.namesrv.NamesrvStartup](../../rocketmq-4.9.0-source/namesrv/src/main/java/org/apache/rocketmq/namesrv/NamesrvStartup.java)


`NamesrvStartup`是`NameServer`的启动类，里面主要做的是从命令行加载配置、加载配置文件，构建了`NamesrvConfig`和`NettyServerConfig`两个配置，然后初始化`NamesrvController`并启动`NamesrvController`

- 从`NamesrvStartup`中可以学习到我们可以使用`org.apache.commons.cli`包来解析命令行的相关参数
- 主要作用1，构建配置类，创建`NamesrvController`：
    ```
    ...
    final NamesrvController controller = new NamesrvController(namesrvConfig, nettyServerConfig);
    ...
    ```
- 主要作用2，调用`NamesrvController`的 start 方法进行启动，并注册了`ShutdownHook`用于在进程退出时调用`NamesrvController`的 shutdown 方法
