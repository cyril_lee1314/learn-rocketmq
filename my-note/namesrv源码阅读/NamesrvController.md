[org.apache.rocketmq.namesrv.NamesrvController](../../rocketmq-4.9.0-source/namesrv/src/main/java/org/apache/rocketmq/namesrv/NamesrvController.java)


`NamesrvController`主要关注'initialize'方法：

```
    public boolean initialize() {

        /**
         * CYRIL-NOTE: 从配置的'kvConfigPath'中加载已持久化的 kvConfig，设置到'kvConfigManager'的成员变量（configTable）中
         */
        this.kvConfigManager.load();

        /**
         * CYRIL-NOTE: 根据 netty 的配置，初始化了一个netty服务
         */
        this.remotingServer = new NettyRemotingServer(this.nettyServerConfig, this.brokerHousekeepingService);

        /**
         * CYRIL-NOTE: 这里创建了一个固定大小的线程池，指定线程名称前缀，用于后面'NettyRequestProcessor'实际处理相关请求
         */
        this.remotingExecutor =
            Executors.newFixedThreadPool(nettyServerConfig.getServerWorkerThreads(), new ThreadFactoryImpl("RemotingExecutorThread_"));

        /**
         * CYRIL-NOTE: 重要. 这里是往'remoteServer'（即NettyServer）注册了一个'NettyRequestProcessor'
         * {@link NettyRequestProcessor}接口主要定义的就是一个处理请求的方法
         * 这里正常应该注册的是{@link DefaultRequestProcessor}，负责处理`NameServer`需要关注的相关请求
         */
        this.registerProcessor();

        /**
         * CYRIL-NOTE: 启动定时任务，每隔10s检测当前未活跃的 Broker
         * 应该是'Broker'启动后会定时发送活跃消息，这里是根据最后一次活跃消息是否超时（定义的超时时间是120s），对已超时的broker判定为已下线
         */
        this.scheduledExecutorService.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                NamesrvController.this.routeInfoManager.scanNotActiveBroker();
            }
        }, 5, 10, TimeUnit.SECONDS);

        /**
         * CYRIL-NOTE: 启动定时任务，每隔10分钟日志输出当前的'kvConfig'
         */
        this.scheduledExecutorService.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                NamesrvController.this.kvConfigManager.printAllPeriodically();
            }
        }, 1, 10, TimeUnit.MINUTES);

        ...
    }
```